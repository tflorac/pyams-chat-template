#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import subprocess

from grp import getgrnam
from pwd import getpwnam


#
# Replace "$((INSTALL))" by install directory for all files
#

TARGET = os.getcwd()

for root, dirs, files in os.walk(TARGET):
    for filename in files:
        # read file content
        with open(os.path.join(root, filename)) as f:
            content = f.read()
        # replace tag by installation path
        content = content.replace('$((INSTALL))', TARGET)
        # replace file content
        with open(os.path.join(root, filename), 'w') as f:
            f.write(content)


#
# Check for logs directory
#

user = '{{ cookiecutter.run_user }}'
user_id = getpwnam(user).pw_uid

group = '{{ cookiecutter.run_group }}'
group_id = getgrnam(group).gr_gid


#
# Check for var directory
#

VAR_DIRECTORY = '{{ cookiecutter.var_directory }}'.replace('$((INSTALL))', TARGET)
LOG_DIRECTORY = '{{ cookiecutter.log_directory }}'.replace('$((INSTALL))', TARGET)

for directory in (VAR_DIRECTORY, LOG_DIRECTORY):
    if not os.path.exists(directory):
        try:
            os.makedirs(directory, mode=0o775, exist_ok=True)
        except PermissionError:
            print("WARNING: Can't create logs directory {0}".format(directory))
        else:
            try:
                os.chown(directory, user_id, group_id)
            except PermissionError:
                print("WARNING: Can't update permission on logs directory {0}".format(directory))


print("\nYour server environment is initialized.")
print("To finalize it's creation:")
print(f" - cd {{ cookiecutter.project_slug }}")
print(f" - sudo ln -s {TARGET}/etc/init.d/gunicorn-{{ cookiecutter.project_slug }}-server /etc/init.d")
print(f" - sudo ln -s {TARGET}/etc/logrotate.d/gunicorn-{{ cookiecutter.project_slug }}-server in /etc/logrotate.d")
print(f" - sudo systemctl daemon-reload")
print(f" - sudo systemctl enable gunicorn-{{ cookiecutter.project_slug }}-server")
print(f" - sudo systemctl start gunicorn-{{ cookiecutter.project_slug }}-server")
