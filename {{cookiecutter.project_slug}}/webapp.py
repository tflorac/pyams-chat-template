
import asyncio
import json
import os

import uvicorn
from pyams_chat_ws.webapp import create_application


base = os.path.dirname(__file__)

with open(os.path.join(base, 'etc', 'config.json'), 'r') as config_file:
    config = json.loads(config_file.read())


loop = asyncio.get_event_loop()
application = loop.run_until_complete(create_application(config))


if __name__ == '__main__':
    uvicorn.run(application, host='{{ cookiecutter.webapp_host }}', port={{ cookiecutter.webapp_port }})
