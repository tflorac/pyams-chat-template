
import logging
import socket


#
# Process settings
#

debug = {{ cookiecutter.debug_mode }}

proc_name = "{{ cookiecutter.project_name }}"
worker_class = 'uvicorn.workers.UvicornWorker'


#
# Server bindings
#

port = {{ cookiecutter.webapp_port }}
bind = [
    '{}:{}'.format(socket.gethostbyname('localhost'), port){%- if cookiecutter.webapp_host != '127.0.0.1' %},,
    '{}:{}'.format(socket.gethostbyname(socket.gethostname()), port){%- endif %}
]

workers = 1
threads = 1

if debug:
    logging.basicConfig(level=logging.DEBUG)
    loglevel = 'debug'
    daemon = False
    accesslog = '-'
    errorlog = '-'
    timeout = 600
else:
    logging.basicConfig(level=logging.WARNING)
    loglevel = 'warning'
    daemon = True
    accesslog = '{{ cookiecutter.log_directory }}/{{ cookiecutter.project_slug }}-access.log'
    errorlog = '{{ cookiecutter.log_directory }}/{{ cookiecutter.project_slug }}-error.log'
    timeout = 30


#
# Logging
#

capture_output = True
